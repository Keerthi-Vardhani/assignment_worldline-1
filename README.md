# Assignment_Worldline-1

#Name: Goli Keerthi Vardhani
#Worldline ID:WDGET2024115
#Email: keerthigoli99@gmail.com

#Simple Form Using get post methods

## Over all steps
1.Ran the project using HTML,CSS,Express js,Node js
2.Cloning the repository in the gitlab using git commands
3.Setting CI/CD pipeline in the gitlab 
4.After Run the repository in the Jenkins

###1.Ran the project using HTML,CSS,Express js,Node js
1.Initially install npm install
2.npm Init
3.Now package.json will be created and need to install express js using command npm install express
4.After completing i wrote index.html file that contain form it includes Name,Password,Age and submit button.
5.After i wrote style.css and linked to index.html file.
6.After completion of html,css i have created app.js where i included port value of 5000 in get method it displays form (index.html)
after i wrote post method that means after entering all values in form after submission of form it will go to thanks.html it displays thanks message.
7.After writing entire code open the browser and type localhost:5000 it will display form containing details of Name,password,Age and after clicking on submit it redirect to page displaying thanks for submitting!

###2.Cloning the repository in the gitlab using git commands
1.intially do git init
2.after check the status usig command git status
3.and then add all files and do commit to save all thos files.
4.after add gitlab repository link using git and then finally pushing the branch into gitlab repository

###3.Setting CI/CD pipeline
1.create the .yml file in the master branch
2.after creating firstly i have wrote 2 jobs build and deploy contains script included necessary installations and the file that is needed to run.
3.After i have wrote stages varaible which will help to control flow of jobs.
4.After that i have included artifacts followed by docker file that is image node which reduces the lined of code to execute.
5.Finally in order to stop background i have included null to execute succesfully.

###4.executing in Jenkins
Taking new file adding git repository and built in jenkins which has been successfully excuted.






